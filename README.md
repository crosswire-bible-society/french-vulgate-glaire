# French Vulgate Glaire

* The French translation of the Vulgate (Latin Bible) by the Abbot Glaire (†1879) with introduction and notes.
* La traduction française de la Vulgate (Bible latine) par l'Abbé Glaire (†1879) avec introduction et notes. Avec les textes Apocryphes de la Vulgate par L’abbé Migne


The OSIS file was build with Arnaud Vié's bible Scraper.

### The translator
* Please visit [Jean-Baptiste Glaire](https://fr.wikipedia.org/wiki/Jean-Baptiste_Glaire) to learn more about the translator.

### Acknowledgement
* Thanks to br cyrille for making the source of this historic work text available on wikisource with facsimiles.


### SWORD notes
* The **TextSource** key in the .conf file is multiline and uses continuation format with \ at EOL.
* When I made the module in October 2017, I was then using Sword utilities in my **Xiphos** path.
* My command line assumes that the OSIS file had been copied to an **Import** folder in my Sword path.

### Method
* Download and install the Biblescraper: https://github.com/UnasZole/bible-scraper
* then run this commands:
`./run.sh scrape -s Generic -i src/main/resources/scrapers/Generic/GlaireWikisource.yaml --fullBible -w OSIS -o frevulgglaire.osis.xml`

`sed -ri 's/<span class="coquille" title="[^<]*">([^<]*)<\/span>/\1/g' tmp/GenericHtml/GlaireWikisource.yaml/*`

`./run.sh scrape -s Generic -i src/main/resources/scrapers/Generic/GlaireWikisource.yaml --fullBible -w OSIS -o frevulgglaire.osis.xml`

Your OSIS is ready

